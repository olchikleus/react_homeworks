import ProductContainer from "../../components/ProductContainer/productContainer";
import '../FavoritesPage/FavoritesPage.css'

const FavoritesPage =({routeProducts, products, favs, onAddFav, onAdd, cartModal}) =>{
    return(
        <main className='container'>
            <h3 className="favs-container__title"> Products in the "Favorites" list</h3>

            {routeProducts.length > 0 ? (
                    < ProductContainer routeProducts={routeProducts}
                                         products={products}
                                         favs={favs}
                                         onAddFav={onAddFav}
                                         onAdd={onAdd}
                                         action={cartModal}
                                         text={"Add to Cart"}
                                         btnClassName="add-to-card"
                    />)
                :
                (<div className='empty'>
                    <h3>You don't have any products in the "Favorites" list yet.</h3>
                    <p>Add the products you like to "Favorites" and you will always have quick access to them.</p>
                </div>)
            }
        </main>
    )
}
export default FavoritesPage;