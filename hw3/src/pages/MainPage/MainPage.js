import ProductContainer from "../../components/ProductContainer/productContainer";


export default function MainPage ({routeProducts, products, favs, onAddFav, onAdd, cartModal}){
    return (
        <div className={'main'} >
            < ProductContainer routeProducts={routeProducts}
                               products={products}
                               favs={favs}
                               onAddFav={onAddFav}
                               onAdd={onAdd}
                               cartModal={cartModal}
                               action={cartModal}
                               text={"Add to Cart"}
                               btnClassName="add-to-card"
            />
        </div>
    )
}