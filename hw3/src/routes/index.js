import {Navigate, Route, Routes} from "react-router-dom";
import Layout from "../components/Layout/Layout";
import MainPage from "../pages/MainPage/MainPage";
import FavoritesPage from "../pages/FavoritesPage/FavoritesPage";
import CartPage from "../pages/CartPage/CartPage";
import React from "react";
export default function Router ({carts, favs, products,  addToFav, openModalAddToCart, cartModal, openModalDeleteFromCart}){
    return(
        <Routes>
            <Route path='/' element={<Layout cartLength={carts.length} favLength={favs.length}/>}>
                <Route index element={<Navigate to={'/main'}/>}/>
                <Route path='/main'
                       element={<MainPage
                           routeProducts={products}
                           products={products}
                           favs={favs}
                           onAddFav={addToFav}
                           onAdd={openModalAddToCart}
                           cartModal={cartModal}/>}
                />
                <Route path='/favorites'
                       element={<FavoritesPage
                           routeProducts={favs}
                           products={products}
                           favs={favs}
                           onAddFav={addToFav}
                           onAdd={openModalAddToCart}
                           cartModal={cartModal}/>}
                />
                <Route path='/carts'
                       element={<CartPage
                           routeProducts={carts}
                           products={products}
                           favs={favs}
                           onAddFav={addToFav}
                           onAdd={openModalDeleteFromCart}
                           cartModal={cartModal}
                       />}
                />
            </Route>
        </Routes>
    )
}

