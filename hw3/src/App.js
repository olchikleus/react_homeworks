import React, {useEffect, useState} from "react";
import "./app.css";
import Modal from "./components/Modal/Modal";
import Router from "./routes";

export  default function App () {
  const [products, setProducts] = useState([])
  const [carts, setCarts] = useState([])
  const [favs, setFavorites] = useState([])
  const [cartModal, setCartModal] = useState(false)
  const [itemToBeAdded, setItemToBeAdded] = useState(null)
  const [deleteCartModal, setDeleteCartModal] = useState(false)
  const [itemToBeRemove, setItemToBeRemove] = useState(null)

    const initStorageData = (products, prop) => {
        const savedCarts = JSON.parse(localStorage.getItem(prop)) || []
        const prods = []
        for (const id of savedCarts) {
            const product = products.find(t => t.article === id)
            prods.push(product)
        }
        switch (prop) {
            case 'carts':
                setCarts([...carts, ...prods])
                break;
            case 'favs':
                setFavorites([...favs, ...prods])
                break;
        }
    }

    useEffect(()=>{
        fetch('/products.json')
            .then(res => res.json())
            .then(products =>{
                setProducts(products)
                initStorageData(products, 'carts')
                initStorageData(products, 'favs')
            })
    }, [])
    function addToFav (product) {
        let result;
        let findElement = favs.find((el)=> el.article === product.article)
        if(findElement){
            result = favs.filter( (el)=> el.article !== product.article)
        } else{
            result = [...favs, product]
        }
        setFavorites(result)
        localStorage.setItem('favs', JSON.stringify(result.map (t => t.article)))
    }
    const addToCart = () => {
        const test =  [...carts, itemToBeAdded]
        setCarts(test)
        setCartModal(false)
        localStorage.setItem('carts', JSON.stringify(test.map(t => t.article)))
    }

    const openModalAddToCart = (product) => {
        setItemToBeAdded(product)
        setCartModal(true)
    }
    const openModalDeleteFromCart = (product) => {
        setItemToBeRemove(product)
        setDeleteCartModal(true)
    }

    const removeFromCart = ()=>{
      const data = carts.filter(item => item.article !== itemToBeRemove.article)
        setCarts(data)
        setDeleteCartModal(false)
        localStorage.setItem('carts', JSON.stringify(data))
    }
    return (
        <>
            {deleteCartModal && (
                  <Modal
                      modalTitle={"Remove product"}
                      modalText={"Do you want to remove this product from cart?"}
                      closeModal={setDeleteCartModal}
                      submitFunction={removeFromCart}
                  />
            )}

            {cartModal && (
                <Modal
                    modalTitle={"Add to Cart"}
                    modalText={"Do you want to add this product to cart?"}
                    closeModal={setCartModal}
                    submitFunction={addToCart}
                />
            )}

            <Router carts={carts}
                    favs={favs}
                    products={products}
                    addToFav={addToFav}
                    openModalAddToCart={openModalAddToCart}
                    cartModal={cartModal}
                    openModalDeleteFromCart={openModalDeleteFromCart}/>
        </>
    );
}