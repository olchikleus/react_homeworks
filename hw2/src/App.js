import React from "react";
import Header from "./components/Header/Header";
import "./app.css";
import Footer from "./components/Footer";
import ProductContainer from "./components/ProductContainer/productContainer";
import {Modal} from "./components/Modal/Modal";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      carts: [],
      favorites: [],
      cartModal: false,
      itemToBeAdded: null,
      products: [],
    };
  }

  initStorageData = (products, prop) => {
    const carts = JSON.parse(localStorage.getItem(prop))
    const prods = []
    for (const id of carts) {
      const product = products.find(t => t.article === id)
      prods.push(product)
    }
    this.setState({[prop]: [...this.state[prop], ...prods]})
  }
  async componentDidMount() {

      await fetch('/products.json')
          .then((res) => res.json())
          .then(products => {
            this.setState(() => ({
              products
            }))
            this.initStorageData(products, 'carts')
            this.initStorageData(products, 'favorites')
          })
  }



    addItemToBeAdded = (product) => {
    this.setState(() => {
      return {
        itemToBeAdded: product,
      };
    });
  };
  openCartModal = (article) => {
    this.setState((state) => {
      return {
        cartModal: true,
      };
    });
  };
  closeModals = () => {
    this.setState((state) => {
      return {
        cartModal: false,
        itemToBeAdded: null
      };
    });
  };
  addToCart = (product) => {
        const carts =  [...this.state.carts, product]
        this.setState({carts})
        localStorage.setItem('carts', JSON.stringify(carts.map(t => t.article)))
  };

    addToFav = (product) => {
        let result;

        const {favorites} = this.state

        let findElement = favorites.find((el)=> el.article === product.article)
        if(findElement){
           result = favorites.filter( (el)=> el.article !== product.article)
        } else{
           result = [...favorites, product]
        }
        this.setState({favorites: result})
        localStorage.setItem('favorites', JSON.stringify(result.map (t => t.article)))
    };
  render() {
    return (
      <div className="main">
          <Header cartLength={this.state.carts.length}
                  favLength={this.state.favorites.length}
        />
        <ProductContainer
          products={this.state.products}
          onAdd={this.addItemToBeAdded}
          onAddFav={this.addToFav}
          openModal={this.openCartModal}
        />

        <Footer />

        {this.state.cartModal && (
          <Modal
            modalTitle={"Add to Cart"}
            modalText={"Do you want to add this product to cart?"}
            closeModal={this.closeModals}
            submitFunction={() => this.addToCart(this.state.itemToBeAdded)}
          />
        )}
      </div>
    );
  }
}

