import React from "react";
import ProductCard from "../ProductCard/productCard";
import "./productContainer.css";
import propTypes from 'prop-types';


export default class ProductContainer extends React.Component {
  render() {
    return (
      <main className="container">
        <ul className="product-card">
          {this.props.products.map((el) => (
            <ProductCard
              key={el.article}
              product={el}
              onAdd={this.props.onAdd}
              openModal={this.props.openModal}
              onAddFav={this.props.onAddFav}
            />
          ))}
        </ul>
      </main>
    );
  }

}


ProductContainer.propTypes = {
    article: propTypes.number,
    products: propTypes.array.isRequired,
    onAdd: propTypes.func.isRequired,
    openModal: propTypes.func.isRequired,
    onAddFav: propTypes.func.isRequired
}