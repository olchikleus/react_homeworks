import { Component } from "react";
import "../Modal/modal.css";
import propTypes from 'prop-types';

export class Modal extends Component {
  render() {
    const { closeModal, submitFunction, modalTitle, modalText} = this.props;

    return (
      <div className="modal" onClick={closeModal}>
        <div className="modal__content" onClick={(e) => e.stopPropagation()}>
          <div className="modal__header">
            <h1 className="modal__title">{modalTitle}</h1>
            <p className="modal__close" onClick={closeModal}>
              &times;
            </p>
          </div>
          <div className="modal__text">
            <p>{modalText}</p>
          </div>
          <div className={"modal__footer"}>
            <div className="action-content">
            <button className={"action-btn"}
              onClick={() => {
                closeModal();
                submitFunction();
              }}
            >
              Ok
            </button>
            <button className={"action-btn"}
                onClick={closeModal}
            >
              Cancel
            </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


Modal.propTypes = {
  closeModal: propTypes.func.isRequired,
  submitFunction: propTypes.func.isRequired,
  modalTitle: propTypes.string.isRequired,
  modalText: propTypes.string.isRequired,
}
