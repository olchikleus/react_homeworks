import React from "react";
import "./productCard.css";
import Button from "../Button/Button";
import propTypes from 'prop-types';
import FavIcon from "../FavoritsIcon/FavIcon";


export default class ProductCard extends React.Component {

  state = {
    modal: false,
    type: false
  };

  clickFav = (e) =>{
    if (!this.state.type) {
      this.setState(()=>{
        return {type: true}
      })
    } else {
      this.setState(()=>{
        return {type: false}
      })
    }
  }
  render() {
    return (

      <li className="product-card__item" id={this.props.product.article}>
        <div className="item__information">
          <h1 className="item__information__title">
            {this.props.product.name}
          </h1>
          <img
            className="item__information__image"
            src={this.props.product.url}
            alt=""
          />
          <p className="item__information__price">
            Price: {this.props.product.price}
          </p>

          <span className="item__information__color"
                style={{ backgroundColor: this.props.product.color }}
          >
          </span>


          <div className="product-card__btn">
            <Button
              text="Add to Cart"
              className="add-to-card"
              onClick={() => {
                this.props.openModal();
                this.props.onAdd(this.props.product);
              }}
            />
            <FavIcon isFav={this.state.type}
                     clickHandler={() => {
                       this.clickFav()
                       this.props.onAddFav(this.props.product)
                     }}
            />

          </div>
        </div>
      </li>
    );
  }
}

ProductCard.propTypes = {
  article: propTypes.number,
  name: propTypes.string,
  price: propTypes.number,
  color: propTypes.string,
  url: propTypes.string,
  onAdd: propTypes.func,
  onAddFav: propTypes.func,
  openModal: propTypes.func,
}