import React from 'react'
import propTypes from 'prop-types'

export default class Button extends React.Component{
    render(){
        return <button {...this.props}>
            {this.props.text}
        </button>
    }
}



Button.propTypes = {
    text: propTypes.oneOfType([propTypes.string, propTypes.node, propTypes.element]).isRequired,
    backgroundColor: propTypes.string,
    className: propTypes.string.isRequired,
}

