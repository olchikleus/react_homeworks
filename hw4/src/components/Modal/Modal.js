import "../Modal/modal.css";
import propTypes from 'prop-types';
import {useDispatch} from "react-redux";
import {hideModalAddToCart} from "../../store/action";

export default function Modal ({closeModal, modalTitle, modalText, submitFunction}) {
    const dispatch = useDispatch()
    return (
        <div className="modal" onClick={closeModal}>
            <div className="modal__content"
                 onClick={(e) => e.stopPropagation()}
            >
              <div className="modal__header">
                  <h1 className="modal__title">{modalTitle}</h1>
                  <p className="modal__close"
                     onClick={() => dispatch(closeModal())}
                  >
                    &times;
                  </p>
              </div>
              <div className="modal__text">
                  <p>{modalText}</p>
              </div>
              <div className={"modal__footer"}>
                  <div className="action-content">
                    <button className={"action-btn"}
                          onClick={() => {
                                submitFunction();
                          }}
                    >
                     Ok
                    </button>
                    <button className={"action-btn"}
                            onClick={() => dispatch(closeModal())}
                    >
                      Cancel
                    </button>
                </div>
              </div>
          </div>
        </div>
    );
}
Modal.propTypes = {
  // closeModal: propTypes.func.isRequired,
  submitFunction: propTypes.func.isRequired,
  modalTitle: propTypes.string.isRequired,
  modalText: propTypes.string.isRequired,
}
