import React, {useEffect, useState} from "react";
import "./app.css";
import Modal from "./components/Modal/Modal";
import Router from "./routes";
import {useDispatch, useSelector} from "react-redux";
import {showModalAddToCart, loadProducts, hideModalAddToCart, showModalDeleteFromCart, hideModalDeleteFromCart} from "./store/action";

export  default function App () {
  const [carts, setCarts] = useState([])
  const [favs, setFavorites] = useState([])
  const [itemToBeAdded, setItemToBeAdded] = useState(null)
  const [itemToBeRemove, setItemToBeRemove] = useState(null)

    const dispatch = useDispatch()

    const{products, cartModal, deleteCartModal} = useSelector(state => ({
        products: state.productsReducer.products,
        cartModal: state.cartModalReducer.cartModal,
        deleteCartModal: state.cartModalReducer.deleteCartModal
    }))

    const initStorageData = (products, prop) => {
        const savedCarts = JSON.parse(localStorage.getItem(prop)) || []
        const prods = []
        for (const id of savedCarts) {
            const product = products.find(t => t.article === id)
            prods.push(product)
        }
        switch (prop) {
            case 'carts':
                setCarts([...carts, ...prods])
                break;
            case 'favs':
                setFavorites([...favs, ...prods])
                break;
        }
    }
    useEffect(()=>{
        dispatch(loadProducts())
        initStorageData(products, 'carts')
        initStorageData(products, 'favs')
    }, [])


    function addToFav (product) {
        let result;
        let findElement = favs.find((el)=> el.article === product.article)
        if(findElement){
            result = favs.filter( (el)=> el.article !== product.article)
        } else{
            result = [...favs, product]
        }
        setFavorites(result)
        localStorage.setItem('favs', JSON.stringify(result.map (t => t.article)))
    }
    const addToCart = () => {
        const test =  [...carts, itemToBeAdded]
        setCarts(test)
        dispatch(hideModalAddToCart())
        localStorage.setItem('carts', JSON.stringify(test.map(t => t.article)))
    }

    const openModalAddToCart = (product) => {
        setItemToBeAdded(product)
        dispatch(showModalAddToCart())
    }
    const openModalDeleteFromCart = (product) => {
        setItemToBeRemove(product)
        dispatch(showModalDeleteFromCart())
    }

    const removeFromCart = ()=>{
      const data = carts.filter(item => item.article !== itemToBeRemove.article)
        setCarts(data)
        dispatch(hideModalDeleteFromCart())
        localStorage.setItem('carts', JSON.stringify(data))
    }
    return (
        <>
            {deleteCartModal && (
                  <Modal
                      modalTitle={"Remove product"}
                      modalText={"Do you want to remove this product from cart?"}
                      closeModal={hideModalDeleteFromCart}
                      submitFunction={removeFromCart}
                  />
            )}

            {cartModal && (
                <Modal
                    modalTitle={"Add to Cart"}
                    modalText={"Do you want to add this product to cart?"}
                    closeModal={hideModalAddToCart}
                    submitFunction={addToCart}
                />
            )}

            <Router carts={carts}
                    favs={favs}
                    products={products}
                    addToFav={addToFav}
                    openModalAddToCart={openModalAddToCart}
                    cartModal={cartModal}
                    openModalDeleteFromCart={openModalDeleteFromCart}
            />
        </>
    );
}