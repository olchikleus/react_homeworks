import {OpenModalAddToCartAction, CloseModalAddToCartAction, OpenModalDeleteFromCartAction, CloseModalDeleteFromCartAction} from './action'

const initialState={
    cartModal: false,
    deleteCartModal: false
}

export default function cartModalReducer(state=initialState, action){
    switch (action.type){
        case "OpenModalAddToCartAction":
            return {...state, cartModal: action.payload}
        case "CloseModalAddToCartAction":
            return {...state, cartModal: action.payload}
        case "OpenModalDeleteFromCartAction":
            return {...state, deleteCartModal: action.payload}
        case "CloseModalDeleteFromCartAction":
            return {...state, deleteCartModal: action.payload}
        default:
            return state
    }
}