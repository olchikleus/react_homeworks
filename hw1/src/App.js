import React from 'react'
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import './app.css'

export default class App extends React.Component{

    state = {
        firstModal: false,
        secondModal: false
    }

    toggleModal = (param)=>{
        this.setState({
            ...this.state,
            [param]: !this.state[param]
        })
    }



    render(){
        return <div className='main-container'>
            <div className='btn-container'>
                        <Button text = "Open first modal" className = "btn btn-first" clickHandler={() => this.toggleModal('firstModal')} />
                        <Button text = "Open second modal" className = "btn btn-second" clickHandler={() => this.toggleModal('secondModal')}/>
            </div>

            <div className='modal-container'>
                {this.state.firstModal &&
                    <Modal header={"Do you want to delete this file?"}
                           text={"Once your delete this file, it won't be possible to undo this action.Are you sure you want to delete it?"}
                           action={<div className='action-content'><button className='action-btn'>Ok</button><button className='action-btn'>Cansel</button></div>}
                           clickHandler={() => this.toggleModal('firstModal')}
                    />
                }
                {this.state.secondModal &&
                    <Modal header={"Do you want to save the changes?"}
                           text={"Make your choice, please!"}
                           action={<div className='action-content'><button className='action-btn2'>Yes</button><button className='action-btn2'>No</button></div>}
                           clickHandler={() => this.toggleModal('secondModal')}
                    />
                }
            </div>
        </div>
    }
}


