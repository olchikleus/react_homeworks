import React from 'react';
import './modal.css'

export default class Modal extends React.Component{

    render() {

        return <div className='modal' onClick={this.props.clickHandler}>
                    <div className='modal__content' onClick={e => e.stopPropagation()}>
                        <div className='modal__header'>
                            <h1 className='modal__title'>{this.props.header}</h1>
                            <p className='modal__close' onClick={this.props.clickHandler}>&times;</p>
                        </div>
                        <div className='modal__text'>
                            <p>{this.props.text}</p>
                        </div>
                        <div className={'modal__footer'}>
                            {this.props.action}
                        </div>
                    </div>
                </div>

    }
}