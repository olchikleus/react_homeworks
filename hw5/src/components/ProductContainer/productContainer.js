import React from "react";
import ProductCard from "../ProductCard/productCard";
import "./productContainer.css";
import propTypes from 'prop-types';
import Button from "../Button";

export default function ProductContainer ({routeProducts,favs, onAdd,onAddFav, btnClassName, text }) {
    // const savedCarts = localStorage.getItem('carts')
    return (
        <div className="product-container">
            <ul className="product-card">
                {routeProducts.map((el) => (
                    <ProductCard
                        key={el.article}
                        product={el}
                        favs={favs}
                        onAdd={onAdd}
                        onAddFav={onAddFav}
                        action={
                            <Button className={btnClassName} text={text}
                                    clickHandler={() => onAdd(el)}
                                    // disabled={savedCarts.includes(el.article)}
                            />
                        }
                    />
                ))}
            </ul>
        </div>
    );
}
ProductContainer.propTypes = {
    article: propTypes.number,
    products: propTypes.array.isRequired,
    onAdd: propTypes.func
}