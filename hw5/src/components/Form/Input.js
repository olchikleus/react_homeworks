import {PatternFormat} from "react-number-format";
import "../../components/Form/form.css"
export default function Input (props) {
    const {field, form, label, ...rest} = props;
    const {name} = field;

    return (
        props.isphoneinput==="true"
            ? (<>
                <div className='form-row'>
                    <label className="form-label">
                        {label}
                        <PatternFormat className="form-control"
                                       format= "+38 (###) ###-##-##"
                                       allowEmptyFormatting mask="_"
                                       {...field} {...rest}
                        />
                    </label>
                    {form.errors[name] && form.touched[name] && <div className='alert'>{form.errors[name]}</div>}
                </div>
                </>
              )
            : (<>
                <div className='form-row'>
                    <label className="form-label">
                        {label}
                        <input className="form-control"
                               {...field} {...rest}
                        />
                    </label>
                    {form.errors[name] && form.touched[name] && <div className='alert'>{form.errors[name]}</div>}
                </div>
                </>)
    )
}
