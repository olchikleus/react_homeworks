import {Form, Formik, Field} from 'formik'
import * as Yup from 'yup'
import Input from "./Input";
import Button from "../Button";
import "../../components/Form/form.css"

export default function FormOfOrdering ({handleSubmit}){
    return(
        <Formik initialValues={{
                    userFirstName: '',
                    userLastName: '',
                    userAge: '',
                    address: '',
                    phoneNumber: ''
                }}

                onSubmit={handleSubmit}
                validationSchema={Yup.object({
                    userFirstName: Yup.string()
                        .matches(/^[A-Za-z ]*$/, 'Please enter valid first name')
                        .required('Enter your first name correctly'),
                    userLastName: Yup.string()
                        .matches(/^[A-Za-z ]*$/, 'Please enter valid last name')
                        .required('Please enter your last name correctly'),
                    userAge: Yup.number()
                        .positive('Enter your age correctly')
                        .integer('Enter your age correctly')
                        .min(18,'Your age must be at least 18 years old')
                        .required('Please enter your age'),
                    address: Yup.string()
                        .required('Please enter full address'),
                    phoneNumber: Yup.string()
                        .min(19, 'Please enter your phone number correctly')
                        .required('Please enter your phone number')
                })}
        >

            <Form className="form-container" noValidate>
                <h3 className='form-title'>Order</h3>
                <Field
                    label="First name"
                    type="text"
                    placeholder="Enter your name"
                    name="userFirstName"
                    component={Input}
                />
                <Field
                    label="Last name"
                    type="text"
                    placeholder="Enter your last name"
                    name="userLastName"
                    component={Input}
                />
                <Field
                    label="Age"
                    type="number"
                    placeholder="Enter your age"
                    name="userAge"
                    component={Input}
                />
                <Field
                    label="Delivery address"
                    type="text"
                    placeholder="Enter your delivery address"
                    name="address"
                    component={Input}
                />
                <Field
                    label="Phone number"
                    type="text"
                    placeholder="+38(___)-__-__-__"
                    name="phoneNumber"
                    component={Input}
                    isphoneinput ="true"
                />
                <div>
                    <Button className='btn-checkout' text="Checkout" type='submit'/>
                </div>
            </Form>
        </Formik>
    )
}