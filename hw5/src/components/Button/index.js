import React from 'react'
import propTypes from 'prop-types'
export default function Button (props){
    return <button onClick={props.clickHandler}
                   style={{backgroundColor: props.backgroundColor}}
                   className={props.className}
    disabled={props.disabled}>
        {props.text}
    </button>
}
Button.propTypes = {
    text: propTypes.oneOfType([propTypes.string, propTypes.node, propTypes.element]),
    backgroundColor: propTypes.string,
    className: propTypes.string,
}

