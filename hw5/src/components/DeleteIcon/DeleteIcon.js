const DeleteIcon = () =>{
    return (<svg viewBox="0 0 14 16"
                 xmlns="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/1999/xlink" focusable="false"  width='25px' height='30px' cursor='pointer'>
        <path fill="#277a1e"
              d="M14 4v1h-1v10.455c0 .3-.224.545-.5.545h-11c-.276 0-.5-.244-.5-.545V5H0V4h14zm-2 1v10H2V5h10zM9.5 0a.5.5 0 01.5.5V2h4v1H0V2h4V.5a.5.5 0 01.5-.5h5zM9 1H5v1h4V1zM5 8h1v4H5V8zm3 0h1v4H8V8z"></path>
    </svg>)
}
export default  DeleteIcon;