import React, {useMemo} from "react";import "./productCard.css";
import Button from "../Button";
import propTypes from 'prop-types';
import FavIcon from "../FavoritsIcon/FavIcon";

export default function ProductCard ({favs, product, action, onAddFav }) {

    const isFav = useMemo(() => !!favs.find(t => t.article === product.article),
        [favs, product]
    )
    return (
        <li className="product-card__item" id={product.article}>
            <div className="item__information">
                <h1 className="item__information__title">
                    {product.name}
                </h1>
                <img
                    className="item__information__image"
                    src={product.url}
                    alt=""
                />
                <p className="item__information__price">Price: {product.price}</p>
                <p>Color:
                    <span className="item__information__color"
                          style={{ backgroundColor: product.color }}>
                </span>
                </p>

                <div className="product-card__btn">
                    {action}
                    <FavIcon  isFav={isFav}
                             clickHandler={() => {
                                 onAddFav(product)
                             }}
                    />
                </div>
            </div>
        </li>
    );

}
ProductCard.propTypes = {
  article: propTypes.number,
  name: propTypes.string,
  price: propTypes.number,
  color: propTypes.string,
  url: propTypes.string,
  onAdd: propTypes.func
}