import {Outlet} from "react-router-dom";
import React from "react";
import Footer from "../Footer";
import Header from "../Header/Header";
const Layout =({cartLength, favLength}) =>{
    return(
        <>
            <Header  cartLength={cartLength}
                     favLength={favLength}
            />
              <Outlet />
            <Footer />
        </>
    )
}
export default Layout;