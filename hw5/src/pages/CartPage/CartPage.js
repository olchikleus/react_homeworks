import React from "react";
import ProductContainer from "../../components/ProductContainer/productContainer";
import '../CartPage/CartPages.css'
import DeleteIcon from "../../components/DeleteIcon/DeleteIcon";
import FormOfOrdering from "../../components/Form/Form";
const CartPage =({routeProducts, products, favs, onAddFav, onAdd, cartModal, handleSubmit}) =>{
    return(
        <main className='container'>
            {routeProducts.length > 0 ? (
                <>
                    <h3 className="cart-container__title">
                        Your shopping cart: selected {routeProducts.length} product(s)
                    </h3>
                    <div className='cart-container'>
                        <div className='cart-container__products'>
                            <ProductContainer routeProducts={routeProducts}
                                           products={products}
                                           favs={favs}
                                           onAddFav={onAddFav}
                                           onAdd={onAdd}
                                           action={cartModal}
                                           text={<DeleteIcon/>}
                                           btnClassName="btn-delete"
                            />
                        </div>
                            <FormOfOrdering handleSubmit={handleSubmit}/>
                    </div>
                </>
                    )
                    :
                    (<div className='empty'>
                        <h3>YOUR SHOPPING CART IS EMPTY!</h3>
                        <p>Look at our catalog and choose the products that interest you.</p>
                    </div>)
            }
        </main>
    )
}
export default CartPage;