export const PRODUCTS = 'PRODUCTS'
export const OpenModalAddToCartAction = 'OpenModalAddToCartAction'
export const CloseModalAddToCartAction = 'CloseModalAddToCartAction'
export const OpenModalDeleteFromCartAction = 'OpenModalDeleteFromCartAction'
export const CloseModalDeleteFromCartAction = 'CloseModalDeleteFromCartAction'
export const loadProducts = () => {
    return async (dispatch) => {
        const res = await fetch('/products.json');
        dispatch({ type: PRODUCTS, payload: await res.json() });
    }
}
export const showModalAddToCart =() =>{
    return (dispatch)=>{
        dispatch({type: OpenModalAddToCartAction, payload: true})
    }
}
export const hideModalAddToCart =() =>{
    return (dispatch)=>{
        dispatch({type: CloseModalAddToCartAction, payload: false})
    }
}

export const showModalDeleteFromCart =() =>{
    return (dispatch)=>{
        dispatch({type: OpenModalDeleteFromCartAction, payload: true})
    }
}

export const hideModalDeleteFromCart =() =>{
    return (dispatch)=>{
        dispatch({type: CloseModalDeleteFromCartAction, payload: false})
    }
}