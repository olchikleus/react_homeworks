import {combineReducers, configureStore} from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import productsReducer from "./productsReducer";
import cartModalReducer from "./modalReducer";
import storage from "redux-persist/lib/storage"
import {persistReducer, persistStore} from "redux-persist";

const persistConfig ={
    key: 'root',
    storage
}

const allReducers = combineReducers({
    productsReducer: productsReducer,
    cartModalReducer: cartModalReducer
})

const persistedReducer = persistReducer(persistConfig, allReducers)


const store = configureStore({
    reducer: persistedReducer,
    middleware: [thunk]
})

// const store = configureStore({
//     reducer: {productsReducer: productsReducer,
//               cartModalReducer: cartModalReducer},
//     middleware: [thunk]
// })
export default store;

export const persistor = persistStore(store);
